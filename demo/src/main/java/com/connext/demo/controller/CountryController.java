package com.connext.demo.controller;

import com.connext.demo.model.Country;
import com.connext.demo.model.ISOCountry;
import com.connext.demo.service.CountryService;
import com.connext.demo.service.ExternalAPIService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/countries")
public class CountryController {

    private static final Log log = LogFactory.getLog(CountryController.class);

    @Autowired
    CountryService countryService;
    @Autowired
    ExternalAPIService externalAPIService;

    @GetMapping("")
    public ResponseEntity<?> list() {
        try {
            List<Country> countries = countryService.getAllCountries();
            return new ResponseEntity<>(countries, HttpStatus.OK);
        } catch (Exception e) {
            String msg = "Error occurred while retrieving countries";
            log.error(msg);
            return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{iso}")
    public ResponseEntity<?> get(@PathVariable String iso) {
        try {
            Country country = countryService.getCountry(iso);
            return new ResponseEntity<>(country, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            String msg = "Error occurred while retrieving countries";
            log.error(msg);
            return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/")
    public ResponseEntity<?> add(@RequestBody Country country) {
        try {
            countryService.saveCountry(country);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            String msg = "Error occurred while retrieving countries";
            log.error(msg);
            return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{iso}")
    public ResponseEntity<?> update(@RequestBody Country country, @PathVariable String iso) {
        try {
            countryService.getCountry(iso);
            country.setIso(iso);
            countryService.saveCountry(country);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            String msg = "Error occurred while retrieving countries";
            log.error(msg);
            return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{iso}")
    public ResponseEntity<?> delete(@PathVariable String iso) {
        try {
            countryService.getCountry(iso);
            countryService.deleteCountry(iso);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            String msg = "Error occurred while retrieving countries";
            log.error(msg);
            return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country-into-by-iso/{iso}")
    public ResponseEntity<?> getCountryByISO(@PathVariable String iso) {
        try {
            Country country = countryService.getCountry(iso);
            ISOCountry isoCountry = externalAPIService.getCountryDetails(country.getName());
            isoCountry.setCountry(country.getName());
            isoCountry.setIso(iso);
            return new ResponseEntity<>(isoCountry, HttpStatus.OK);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            String msg = "Error occurred while retrieving country by iso " + iso;
            log.error(msg);
            return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
