package com.connext.demo.service;

import com.connext.demo.model.Country;
import com.connext.demo.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;

    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    public Country getCountry(String iso) {
        return countryRepository.findById(iso).get();
    }

    public void saveCountry(Country country) {
        countryRepository.save(country);
    }

    public void deleteCountry(String iso) {
        countryRepository.deleteById(iso);
    }
}
