package com.connext.demo.service;

import com.connext.demo.model.ISOCountry;
import okhttp3.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
public class ExternalAPIService {

    private final String BASE_URL = "https://countriesnow.space/api/v0.1/countries";

    public ISOCountry getCountryDetails(String countryName) throws IOException, JSONException {
        OkHttpClient client = new OkHttpClient();

        RequestBody formBody = new FormBody.Builder().add("country", countryName).build();

        Request request = new Request.Builder().url(BASE_URL + "/capital").post(formBody).build();
        Response response = client.newCall(request).execute();
        JSONObject jsonObject;
        String capital = "";
        try (ResponseBody responseBody = response.body()) {
            String payload = Objects.requireNonNull(responseBody).string();
            jsonObject = new JSONObject(payload).getJSONObject("data");
            capital = jsonObject.getString("capital");
        }
        response.close();

        request = new Request.Builder().url(BASE_URL + "/currency").post(formBody).build();
        response = client.newCall(request).execute();
        String currency = "";
        try (ResponseBody responseBody = response.body()) {
            String payload = Objects.requireNonNull(responseBody).string();
            jsonObject = new JSONObject(payload).getJSONObject("data");
            currency = jsonObject.getString("currency");
        }
        response.close();

        ISOCountry isoCountry = new ISOCountry();
        isoCountry.setCapital(capital);
        isoCountry.setCurrency(currency);
        return isoCountry;
    }
}
